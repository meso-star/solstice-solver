/* Copyright (C) 2018, 2019, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2016, 2018 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SSOL_MATERIAL_C_H
#define SSOL_MATERIAL_C_H

#include <rsys/ref_count.h>

struct s3d_hit;
struct s3d_primitive;
struct ssf_bsdf;
struct ssol_device;

struct dielectric {
  int dummy;
};

struct matte {
  ssol_shader_getter_T reflectivity;
};

struct mirror {
  ssol_shader_getter_T reflectivity;
  ssol_shader_getter_T roughness;
  enum ssol_microfacet_distribution distrib;
};

struct thin_dielectric {
  struct ssol_medium slab_medium;
  double thickness;
};

struct ssol_material {
  enum ssol_material_type type;

  ssol_shader_getter_T normal;

  union {
    struct dielectric dielectric;
    struct matte matte;
    struct mirror mirror;
    struct thin_dielectric thin_dielectric;
  } data;

  struct ssol_medium out_medium;
  struct ssol_medium in_medium;

  struct ssol_param_buffer* buf;
  struct ssol_device* dev;
  ref_T ref;
};

extern LOCAL_SYM void
surface_fragment_setup
  (struct ssol_surface_fragment* fragment,
   const double pos[3],
   const double dir[3],
   const double normal[3],
   const struct s3d_primitive* primitive,
   const float uv[2]);

extern LOCAL_SYM void
material_shade_normal
  (const struct ssol_material* mtl,
   const struct ssol_surface_fragment* fragment,
   const double wavelength,
   double N[3]);

extern LOCAL_SYM res_T
material_create_bsdf
  (const struct ssol_material* mtl,
   const struct ssol_surface_fragment* fragment,
   const double wavelength, /* In nanometer */
   const struct ssol_medium* medium, /* Current medium */
   const int rendering, /* Is material used for rendering purposes */
   struct ssf_bsdf** bsdf); /* Bidirectional Scattering Distribution Function */

extern LOCAL_SYM res_T
material_get_next_medium
  (const struct ssol_material* mtl,
   const struct ssol_medium* medium, /* Current mediu */
   struct ssol_medium* next_medium);

extern LOCAL_SYM int
media_ceq(const struct ssol_medium* a, const struct ssol_medium* b);

#endif /* SSOL_MATERIAL_C_H */

